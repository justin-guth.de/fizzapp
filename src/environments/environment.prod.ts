export const environment = {
  production: true,

  apiBaseUrl: 'https://fizz.justin-guth.de/api/',
  apiGetDrinksListEndpoint: 'drinks',
  apiCreateTokenEndpoint: 'tokens/create',

  localStorageKeys: {
    apiToken: 'fizz_apiToken'
  }
};
