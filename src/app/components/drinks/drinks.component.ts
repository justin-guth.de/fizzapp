import { EscapeRegexService } from './../../services/escape-regex.service';
import { Drink } from './../../types/drink';
import { DrinksService } from './../../services/drinks.service';
import { Component, OnInit } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.scss'],
})
export class DrinksComponent implements OnInit {
  public drinks: Drink[];
  public allDrinks: Drink[];

  constructor(
    private drinksService: DrinksService,
    private escapeRegexService: EscapeRegexService
  ) {}

  ngOnInit() {
    this.drinksService.getDrinksList((drinks: Drink[]) => {
      this.drinks = drinks;
      this.allDrinks = drinks;
    });
  }

  purchase(id: string) {
    console.log('Purchasing drink:', id);
  }

  performSearch(event: Event) {
    const searchTerm: string = (event.target as unknown as IonSearchbar).value;

    if (searchTerm === '' || searchTerm === null || searchTerm === undefined) {
      this.drinks = this.allDrinks;
      return;
    }

    const pattern = new RegExp(`.*${searchTerm}.*`);
    this.drinks = this.allDrinks.filter(
      (drink: Drink) => drink.name.match(pattern)
    );
  }
}
