import { Component, ViewChild } from '@angular/core';
import { IonInput } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('itemInput') itemInput: IonInput;

  public items: string[] = [

    'Eier',
    'Milch',
    'Brokkoli'
  ];

  constructor() { }

  addItem() {

    console.log('Adding item...', this.itemInput.value);
    this.items.push(this.itemInput.value as string);

  }

}
