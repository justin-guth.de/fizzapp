export interface CreateTokenBody {
  email: string;
  password: string;
  // eslint-disable-next-line @typescript-eslint/naming-convention
  device_name: string;
  remember?: boolean;
}
