export interface ApiResponse {

  // eslint-disable-next-line @typescript-eslint/naming-convention
  is_error: boolean;
  data?: any;
  errors?: any;
}
