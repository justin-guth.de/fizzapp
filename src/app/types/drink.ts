export interface Drink {

  id: string;
  name: string;
  cost: number;
  stock: number;
}
