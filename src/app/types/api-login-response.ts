import { ApiResponse } from './api-response';
export interface ApiLoginResponse extends ApiResponse{
  data?: {
    token: string;
  };
}
