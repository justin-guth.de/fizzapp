import { registerLocaleData } from '@angular/common';
import { Component } from '@angular/core';
import localeDeExtra from '@angular/common/locales/extra/de';
import localeDe from '@angular/common/locales/de';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor() {

    registerLocaleData(localeDe, 'de-DE', localeDeExtra);
  }
}
