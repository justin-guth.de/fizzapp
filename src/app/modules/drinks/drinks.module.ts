import { IonicModule } from '@ionic/angular';
import { DrinksComponent } from 'src/app/components/drinks/drinks.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [DrinksComponent],
  exports: [DrinksComponent],
  imports: [
    CommonModule,
    IonicModule,
  ]
})
export class DrinksModule { }
