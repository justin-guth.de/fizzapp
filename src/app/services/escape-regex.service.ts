import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class EscapeRegexService {
  constructor() {}

  escape(text: string) {
    return text.replace(/[.*+?^${}()|[]\]/g, '$&');
  }
}
