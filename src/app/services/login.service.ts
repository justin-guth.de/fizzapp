import { ApiLoginResponse } from './../types/api-login-response';
import { CreateTokenBody } from './../types/create-token-body';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  isLoggedIn(): boolean {
    const token: string = this.getToken();
    return token !== null && token !== undefined;
  }

  getToken(): string {
    return localStorage.getItem(environment.localStorageKeys.apiToken);
  }

  login(
    email: string,
    password: string,
    onLoginSuccess: (data: any) => void = null,
    onLoginFailure: (errors: any) => void = null
  ): void {
    const body: CreateTokenBody = {
      email,
      password,
      // eslint-disable-next-line @typescript-eslint/naming-convention
      device_name: 'fizzApp',
    };

    this.http
      .post(environment.apiBaseUrl + environment.apiCreateTokenEndpoint, body)
      .subscribe((response: ApiLoginResponse) => {
        if (response.is_error) {
          localStorage.setItem(environment.localStorageKeys.apiToken, null);
          if (onLoginFailure !== null) {
            onLoginFailure(response.errors);
          }
        } else {
          localStorage.setItem(environment.localStorageKeys.apiToken, response.data.token);
          if (onLoginSuccess !== null) {
            onLoginSuccess(response.data);
          }
        }
      });
  }
}
