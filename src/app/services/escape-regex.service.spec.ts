import { TestBed } from '@angular/core/testing';

import { EscapeRegexService } from './escape-regex.service';

describe('EscapeRegexService', () => {
  let service: EscapeRegexService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EscapeRegexService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
