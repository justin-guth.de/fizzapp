import { Drink } from './../types/drink';
import { ApiResponse } from './../types/api-response';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DrinksService {

  constructor(private http: HttpClient) { }

  getDrinksList(then: (drinks: Drink[]) => void): void {

    this.http.get(environment.apiBaseUrl + environment.apiGetDrinksListEndpoint).subscribe(
      (response: ApiResponse) => {

        if (response.is_error) {
          console.error(response.errors);
          then(null);
        }
        else {
          then(response.data);
        }
      }
    );
  }
}
